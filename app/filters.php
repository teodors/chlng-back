<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	if($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
		$statusCode = 204;

		$headers = [
		    'Access-Control-Allow-Origin'  => '*',
		    'Access-Control-Allow-Methods' => 'GET, POST, OPTIONS, DELETE, PATCH',
		    'Access-Control-Allow-Headers' => 'Origin, Content-Type, Accept, Authorization, X-Requested-With',
		    'Access-Control-Allow-Credentials' => 'true'
		];

		return Response::make(null, $statusCode, $headers);
	}
});


App::after(function($request, $response)
{
    $response->headers->set('Access-Control-Allow-Origin', '*');
    $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE, PATCH');
    $response->headers->set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, X-Requested-With');
    $response->headers->set('Access-Control-Allow-Credentials', 'true');
    return $response;
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});

Route::filter('auth.token', function()
{

	/* 
	*  Check if username and token are provided.
	*  If both are not provided or not existing in database, give error.
	*/

	$token = Input::get('token');

	if($token != NULL && $token != '') {
		$result = DB::table('users')
            ->where('token', '=', Input::get('token'))
            ->first();
	
		if($result) {
			return;
		}

	}

	$content = array(
		'error' => true,
		'access' => false,
		'messages' => array("You don't have access."),
	);

    $response = Response::json($content, 401);
	$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

	return $response;

});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});