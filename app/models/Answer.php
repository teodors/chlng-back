<?php

class Answer extends \Eloquent {
	protected $fillable = [];

	private $rules = array(
	    'description'=>'required',
	    'challenge_id'=>'required'
    );

	/**
	 * Stored error messages by validator.
	 * @var Array
	 */
    public $errors;

    /**
     * Validates input data.
     * @param  Array $data Collected all input data.
     * @return Bool       Depending on validation returns true or false.
     */
 	public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);	

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->messages()->all();
            return false;
        }

        // validation pass
        return true;
    }

    /**
	 * Get the errors of validation.
	 *
	 * @return array
	 */
	public function getErrors()
    {
        return $this->errors;
    }
}