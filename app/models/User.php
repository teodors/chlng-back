<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	private $rules = array(
	    'username'=>'required|alpha|min:2|unique:users,username',
	    'name'=>'required|min:2',
	    'email'=>'required|email|unique:users,email',
	    'bio'=>'max:300',
	    'web'=>'max:255',
	    'location'=>'max:255',
	    'picture'=>'max:255',
	    'password'=>'required|alpha_num|between:6,12'
    );

	// Returns validation errors
    public $errors;

	/**
	 * Validates User model data.
	 *
	 * @var string
	 */
 	public function validate($data, $user_id = NULL)
    {
        // make a new validator object
        // user_id depends weather user already exists or is registering
        if($user_id) {
        	$new_rules = $this->rules;
        	$new_rules['username'] = 'required|alpha|min:2|unique:users,username,' . $user_id;
        	$new_rules['email'] = 'required|email|unique:users,email,' . $user_id;
        	$new_rules['password'] = '';
        	$v = Validator::make($data, $new_rules);
        } else {
        	$v = Validator::make($data, $this->rules);	
        }
        

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->messages()->all();
            return false;
        }

        // validation pass
        return true;
    }

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'token');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get all users that arr following current user.
	 * @return mix
	 */
	public function getFollowers()
	{
		return $this->belongsToMany('User', 'followers', 'user_to', 'user_from');
	}

	/**
	 * Get users that current user is following.
	 * @return mix
	 */
	public function getFollowing()
	{
		return $this->belongsToMany('User', 'followers', 'user_from', 'user_to');
	}

	/**
	 * Get challenges.
	 * @return mix
	 */
	public function getChallenges()
	{
		return $this->hasMany('Challenge', 'author_id');
	}

	/**
	 * Get the errors of validation.
	 *
	 * @return array
	 */
	public function getErrors()
    {
        return $this->errors;
    }

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	 * Get token
	 * @return type
	 */
	public function getRememberToken()
	{
	    return $this->token;
	}

	/**
	 * Set token
	 * @return type
	 */
	public function setRememberToken($value)
	{
	    $this->token = $value;
	}

	/**
	 * Get token name
	 * @return type
	 */
	public function getRememberTokenName()
	{
	    return 'token';
	}

}