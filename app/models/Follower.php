<?php

class Follower extends \Eloquent {
	protected $fillable = [];

    private $rules = array(
        'user_to'=>'required|exists:users,id',
	    'user_from'=>'required|exists:users,id'
    );

    // Returns validation errors
    public $errors;

    /**
	 * Validates User model data.
	 *
	 * @var string
	 */
 	public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);	

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->messages()->all();
            return false;
        }

        if (DB::table('followers')->where('user_to','=', $data['user_to'])->where('user_from','=', $data['user_from'])->get() != NULL) {
        	// set errors and return false
            $this->errors = array('You have already followed this user!');
            return false;
        }

        // validation pass
        return true;
    }

    public function getFollowers()
    {
        return $this->belongsTo('User');
    }

    /**
	 * Get the errors of validation.
	 *
	 * @return array
	 */
	public function getErrors()
    {
        return $this->errors;
    }

}