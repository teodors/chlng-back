<?php

class Vote extends \Eloquent {
	protected $fillable = [];

	public function user()
    {
        return $this->belongsTo('User');
    }

    private $rules = array(
        'item_id'=>'required',
	    'is_challenge'=>'required'
    );

    // Returns validation errors
    public $errors;

    /**
	 * Validates User model data.
	 *
	 * @var string
	 */
 	public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);	

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->messages()->all();
            return false;
        }

        if (DB::table('votes')->where('item_id','=', $data['item_id'])->where('user_id','=', $data['user_id'])->where('is_challenge','=', $data['is_challenge'])->get() != NULL) {
        	// set errors and return false
            $this->errors = array('You have already voted!');
            return false;
        }

        // validation pass
        return true;
    }

    /**
	 * Get the errors of validation.
	 *
	 * @return array
	 */
	public function getErrors()
    {
        return $this->errors;
    }

}