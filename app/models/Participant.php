<?php

class Participant extends \Eloquent {
	protected $fillable = [];

	public function user()
    {
        return $this->belongsTo('Challenge');
    }

    private $rules = array(
        'challenge_id'=>'required|exists:challenges,id',
	    'user_id'=>'required|exists:users,id'
    );

    // Returns validation errors
    public $errors;

    /**
	 * Validates User model data.
	 *
	 * @var string
	 */
 	public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);	

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->messages()->all();
            return false;
        }

        if (DB::table('votes')->where('challenge_id','=', $data['challenge_id'])->where('user_id','=', $data['user_id'])->get() != NULL) {
        	// set errors and return false
            $this->errors = array('You have already voted!');
            return false;
        }

        // validation pass
        return true;
    }

    /**
	 * Get the errors of validation.
	 *
	 * @return array
	 */
	public function getErrors()
    {
        return $this->errors;
    }

}