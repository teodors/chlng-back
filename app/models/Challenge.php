<?php

class Challenge extends \Eloquent {
	protected $fillable = [];

	private $rules = array(
	    'description'=>'required'
    );

	// Returns validation errors
    public $errors;

    /**
	 * Validates User model data.
	 *
	 * @var string
	 */
 	public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);	

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->messages()->all();
            return false;
        }

        // validation pass
        return true;
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
	 * Get the errors of validation.
	 *
	 * @return array
	 */
	public function getErrors()
    {
        return $this->errors;
    }
}