<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


// Public info page. (not API)
Route::get('/', 'HomeController@index');

// User authentication.
Route::post('login', 'UsersController@login');
Route::get('facebook', 'UsersController@loginWithFacebook');
Route::post('register', 'UsersController@store');

// Users - Gravatar
Route::get('gravatar/{hash}', 'UsersController@gravatar');


// Available only to authorised users.
Route::group(array('before' => 'auth.token'), function() 
{
	// Still needs followers challenges, my challanges, trending challenges
	Route::get('/challenges/votes', 'ChallengesController@votes');
	Route::get('/challenges/me/{id}', 'ChallengesController@me');
	Route::get('/challenges/participating/{id}', 'ChallengesController@participating');
	Route::resource('challenges', 'ChallengesController',
		array('except' => array('create', 'edit')));

	// Search
	Route::resource('search', 'SearchController',
		array('only' => array('show')));

	// Votes
	Route::resource('votes', 'VotesController',
		array('only' => array('store', 'destroy')));

	// Answers
	Route::resource('answers', 'AnswersController',
		array('except' => array('create', 'edit')));

	// Followers
	Route::resource('followers', 'FollowersController',
		array('only' => array('show', 'store', 'destroy')));

	// Users
	Route::resource('users', 'UsersController',
		array('except' => array('create', 'edit')));

	// Users - Logout
	Route::get('logout', 'UsersController@logout');
});

	

