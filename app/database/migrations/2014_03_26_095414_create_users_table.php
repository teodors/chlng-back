<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('username', 120)->unique();
			$table->string('name', 100);
			$table->text('bio')->nullable();
			$table->string('email', 255)->unique();
			$table->string('password', 60);
			$table->string('picture', 255)->nullable();
			$table->string('location', 255)->nullable();
			$table->string('web', 255)->nullable();
			$table->string('token', 255)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
