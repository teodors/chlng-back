<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('followers', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_to')->unsigned()->index();
			$table->integer('user_from')->unsigned()->index();
			$table->timestamps();

			$table->foreign('user_to')->references('id')->on('users')->on_update('cascade')->on_delete('cascade');
			$table->foreign('user_from')->references('id')->on('users')->on_update('cascade')->on_delete('cascade');		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('followers');
	}

}
