<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		
		$users = [
			['username' => 'janis', 
			 'password' => Hash::make('qwerty'),
			 'name' => 'Jānis Ozoliņš',
			 'bio' => 'Es esmu stilīgs.',
			 'email' => 'janis.ozolins@gmail.com',
			 'web' => 'http://janis.lv',
			 'picture' => 'https://s3.amazonaws.com/uifaces/faces/twitter/catarino/128.jpg',
			 'location' => 'Latvija, Rīga',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['username' => 'teodors', 
			 'password' => Hash::make('qwerty'),
			 'name' => 'Teodors Zeltiņš',
			 'bio' => 'Māku iekārdināt.',
			 'email' => 'teodors.zeltins@gmail.com',
			 'web' => 'http://teodors.lv',
			 'picture' => 'https://s3.amazonaws.com/uifaces/faces/twitter/michzen/128.jpg',
			 'location' => 'Latvija, Rīga',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['username' => 'zigfrids', 
			 'password' => Hash::make('qwerty'),
			 'name' => 'Zigfrīds Pērlīte',
			 'bio' => 'Esmu ļoti garšs.',
			 'email' => 'zigfrids.perlite@gmail.com',
			 'web' => 'http://teodors.lv',
			 'picture' => 'https://s3.amazonaws.com/uifaces/faces/twitter/bajazetov/128.jpg',
			 'location' => 'Latvija, Rīga',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')]
		];

		DB::table('users')->insert($users);

	}

}