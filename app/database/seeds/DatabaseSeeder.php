<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

			$this->call('UsersTableSeeder');
			$this->call('ChallengesTableSeeder');
			$this->call('FollowersTableSeeder');
			$this->call('VotesTableSeeder');
			$this->call('AnswersTableSeeder');
	}

}