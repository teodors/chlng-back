<?php

class AnswersTableSeeder extends Seeder {

	public function run()
	{
		
		$answers = [
			['user_id' => '2', 
			 'challenge_id' => '1', 
			 'description' => 'Desmit punkti Grifindoram!', 
			 'is_challenge' => '1', 
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_id' => '2', 
			 'challenge_id' => '2', 
			 'description' => 'Labs komentārs.', 
			 'is_challenge' => '1', 
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_id' => '2', 
			 'challenge_id' => '3', 
			 'description' => 'Esmu iebruku Etiopijā!', 
			 'is_challenge' => '1', 
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_id' => '2', 
			 'challenge_id' => '4', 
			 'description' => 'Sunshine reggae!',
			 'is_challenge' => '1',  
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_id' => '1', 
			 'challenge_id' => '5', 
			 'description' => 'Let the good vibes - get a lot stronger!', 
			 'is_challenge' => '1', 
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],
		];

		DB::table('answers')->insert($answers);

	}

}