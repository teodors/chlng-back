<?php

class ChallengesTableSeeder extends Seeder {

	/**
	 * Seeds database and creates sample challenges.
	 */
	public function run()
	{
		$challenges = [
			['author_id' => '1', 
			 'description' => 'Nostāvi uz rokām 10 minūtes.',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['author_id' => '2', 
			 'description' => 'Izdzer terminatoru.',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['author_id' => '3', 
			 'description' => 'Nolaizi sabiedriskā transporta stieni.',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['author_id' => '2', 
			 'description' => 'Atzīsties mīlestībā Ilzei.',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['author_id' => '3', 
			 'description' => 'Izdzer piena paku vienā metienā.',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['author_id' => '1', 
			 'description' => 'Nogriez matus uz nullīti.',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['author_id' => '2', 
			 'description' => 'Apskrien apkārt savai mājai pa pliko.',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['author_id' => '3', 
			 'description' => 'Izlec ar izpletni.',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],
		];

		DB::table('challenges')->insert($challenges);
	}

}