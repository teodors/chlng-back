<?php

class VotesTableSeeder extends Seeder {

	public function run()
	{
		
		$votes = [
			['user_id' => '2', 
			 'item_id' => '1', 
			 'is_challenge' => '1', 
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_id' => '2', 
			 'item_id' => '2', 
			 'is_challenge' => '1',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_id' => '2', 
			 'item_id' => '3', 
			 'is_challenge' => '1',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_id' => '1', 
			 'item_id' => '3', 
			 'is_challenge' => '1',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_id' => '3', 
			 'item_id' => '3', 
			 'is_challenge' => '1',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_id' => '3', 
			 'item_id' => '3', 
			 'is_challenge' => '1',
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],
		];

		DB::table('votes')->insert($votes);

	}

}