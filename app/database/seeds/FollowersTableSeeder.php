<?php

class FollowersTableSeeder extends Seeder {

	public function run()
	{
		
		$followers = [
			['user_to' => '1', 
			 'user_from' => '2', 
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_to' => '2', 
			 'user_from' => '3', 
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_to' => '3', 
			 'user_from' => '1', 
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_to' => '3', 
			 'user_from' => '2', 
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],

			['user_to' => '1', 
			 'user_from' => '3', 
			 'created_at' => date('Y-m-d H:i:s'),
			 'updated_at' => date('Y-m-d H:i:s')],
		];

		DB::table('followers')->insert($followers);

	}

}