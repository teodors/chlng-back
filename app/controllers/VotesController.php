<?php

class VotesController extends \BaseController {


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$input['user_id'] = DB::table('users')->where('token', '=', $input['token'])->pluck('id');
		$votes = new Vote;

		// process the login
		if ($votes->validate($input)) {

			// store
			$votes->user_id = $input['user_id'];
			$votes->item_id = $input['item_id'];
			$votes->is_challenge = $input['is_challenge'];
			$votes->save();

			// success response
			$content = array(
				'error' => false,
				'messages' => array('Vote added!'),
			);

		} else {

			// fail response
			$content = array(
				'error' => true,
				'messages' => $votes->getErrors()
			);

		}

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Removes current users vote from specified challanges.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$current = DB::table('users')
				->where('token', '=', Input::get('token'))
				->pluck('id');
		$votes = DB::table('votes')
				->where('item_id', '=', $id)
				->where('user_id', '=', $current)
				->where('is_challenge', '=', Input::get('is_challenge'))
				->pluck('id');

		if($votes != NULL) {
			Vote::find($votes)->delete();
			$content = array(
				'error' => false,
				'messages' => array('Vote removed!')
			);
		} else {
			$content = array(
				'error' => true,
				'messages' => array('No vote to remove!')
			);
		}

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

}