<?php

class ChallengesController extends \BaseController {

	/**
	 * Display 20 recent (user and users he is following) challenges.
	 *
	 * @return Response
	 */
	public function index()
	{

		// if date set return results only from that date
		if(Input::get('date') == NULL) {
			$req_date = date('Y-m-d H:i:s');
		} else {
			$req_date = Input::get('date');
		}

		// return challenges from him and users he is following
        $results = DB::table('challenges') // A
        	->select('challenges.id', 
        		'challenges.description', 
        		'challenges.created_at', 
        		'challenges.updated_at', 
        		'users.id as user_id', 
        		'users.username', 
        		'users.name', 
        		'users.picture',
        		DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id = challenges.id AND votes.is_challenge = 1) as votes'),     	
        		DB::raw('(SELECT COUNT(*) FROM answers WHERE answers.challenge_id = challenges.id) as answers'))      	
	        ->leftJoin('users', 'users.id', '=', 'challenges.author_id')
		    ->whereIn('author_id', function($query)
		    {
		    	// select only users he is following
		        $query->select('user_to')
	              	->from('followers')
	              	->where('followers.user_from', '=', function($query){
	              		$query->select('id')
			              	->from('users')
			              	->where('users.token', '=', Input::get('token'))
			              	->first();
	              	})
	              	->get();
		    })
		    // and himself
		    ->orWhere( 'author_id', '=', function($query){
          		$query->select('id')
	              	->from('users')
	              	->where('users.token', '=', Input::get('token'))
	              	->first();
			    })
		    ->where('challenges.created_at', '<=', $req_date)
		    ->orderBy('challenges.created_at', 'desc')
		    ->skip(Input::get('skip'))
		    ->take(20)		    
		    ->get();

			$content = array(
				'error' => false,
				'messages' => array(),
				'challenges' => $results,
			);

		$response = Response::json($content, 200);
		return $response;
	}

	/**
	 * Display 20 popular (user and users he is following) challenges. 
	 * Implements Hacker News algorithm for popularity.
	 *
	 * @return Response
	 */
	public function votes()
	{

		// if date set return results only from that date
		if(Input::get('date') == NULL) {
			$req_date = date('Y-m-d H:i:s');
		} else {
			$req_date = Input::get('date');
		}

		// returns all of current user followers challenges and challenge responses 
        $results = DB::table('challenges') // A
        	->select('challenges.id', 
        		'challenges.description', 
        		'challenges.created_at', 
        		'challenges.updated_at', 
        		'users.id as user_id', 
        		'users.username', 
        		'users.name', 
        		'users.picture',
        		DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id = challenges.id AND votes.is_challenge = 1) as votes'),     	
        		DB::raw('(SELECT COUNT(*) FROM answers WHERE answers.challenge_id = challenges.id ) as answers'))      	
	        ->leftJoin('users', 'users.id', '=', 'challenges.author_id')
		    ->where('challenges.created_at', '<=', $req_date)
		    // Hacker News algorithm
		    ->orderByRaw('((votes-1)/POW(((UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(challenges.created_at))/3600)+2,1.5)) DESC')
		    ->skip(Input::get('skip'))
		    ->take(20)		    
		    ->get();

			$content = array(
				'error' => false,
				'messages' => array(),
				'challenges' => $results,
			);

		$response = Response::json($content, 200);
		return $response;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function participating($id)
	{

		// if date set return results only from that date
		if(Input::get('date') == NULL) {
			$req_date = date('Y-m-d H:i:s');
		} else {
			$req_date = Input::get('date');
		}

		// returns all of current user followers challenges and challenge responses 
        $results = DB::table('challenges') // A
        	->select('challenges.id', 
        		'challenges.description', 
        		'challenges.created_at', 
        		'challenges.updated_at', 
        		'users.id as user_id', 
        		'users.username', 
        		'users.name', 
        		'users.picture',
        		//select votes
        		DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id = challenges.id AND votes.is_challenge = 1) as votes'),     	
        		//select answers
        		DB::raw('(SELECT COUNT(*) FROM answers WHERE answers.challenge_id = challenges.id ) as answers'))      	
	        ->leftJoin('users', 'users.id', '=', 'challenges.author_id')
		    ->whereIn('challenges.id', function($query) use ($id)
		    {
		        $query->select('challenge_id')
	              	->from('participants')
	              	->where('user_id', '=', $id);
		    })
		    ->orderBy('challenges.created_at', 'desc')
		    ->skip(Input::get('skip'))
		    ->take(20)		    
		    ->get();

			$content = array(
				'error' => false,
				'messages' => array(),
				'challenges' => $results,
			);

		$response = Response::json($content, 200);
		return $response;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$other = Input::json()->all();
		$challenge = new Challenge;

		// process the login
		if ($challenge->validate($other)) {

			// store
			$challenge->author_id = DB::table('users')->where('token', '=', $input['token'])->pluck('id');
			$challenge->description = $other['description'];
			$challenge->save();

			// get participants from a challenge and put them in their table
			preg_match_all("/\@([a-z0-9_]+)/i", $challenge->description, $participants);
			for ($i = 0; $i < count($participants[1]); ++$i) {
				$new = new Participant;
				$new->user_id = DB::table('users')->where('username', '=', $participants[1][$i])->pluck('id');
				$new->challenge_id = $challenge->id;
				$new->save();
			}

			// success response
			$content = array(
				'error' => false,
				'messages' => array('New challenge created!'),
				'challenges' => array('author_id' => $challenge->author_id, 'description' => $challenge->description)
			);

		} else {

			// fail response
			$content = array(
				'error' => true,
				'messages' => $challenge->getErrors()
			);	

		}

		$response = Response::json($content, 200);
		return $response;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user_id = DB::table('users')
						->select('users.id')
						->where('users.token','=', Input::get('token'))
						->first();
		// select current challenge and votes 
		$results = DB::table('challenges')
						->select('challenges.id', 
							'challenges.description',
							'challenges.created_at',
							'challenges.updated_at',
							'users.id as user_id',
							'users.username',
							'users.name',
							'users.picture',
							DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id = challenges.id AND votes.is_challenge = 1) as votes'),
							DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id = challenges.id AND votes.user_id = ' . $user_id->id . ' AND votes.is_challenge = 1) as voted')
							)
						->where('challenges.id', '=', $id)
						->leftJoin('users', 'users.id', '=', 'challenges.author_id')
						->get();

		// select all current challenge answers and votes 
		$answers = DB::table('answers')
						->select('answers.id', 
							'answers.description',
							'answers.created_at',
							'answers.updated_at',
							'users.id as user_id',
							'users.username',
							'users.name',
							'users.picture',
							DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id = answers.id AND votes.is_challenge = 0) as votes'),
							DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id = answers.id AND votes.user_id = ' . $user_id->id . ' AND votes.is_challenge = 0) as voted')
							)
						->where('challenge_id', '=', $id)
						->leftJoin('users', 'users.id', '=', 'answers.user_id')
						->get();

		$content = array(
			'error' => false,
			'messages' => array(),
			'challenges' => $results,
			'answers' => $answers
		);

		$response = Response::json($content, 200);
		return $response;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$challenge = Challenge::find($id);

		$current = DB::table('users')->where('token', '=', $input['token'])->pluck('id');

		// if challenge data validates and challenge is owned by current user then proceed
		if ($challenge->validate($input) && $challenge->author_id == $current) {

			// store
			$challenge->description = Input::get('description');
			$challenge->save();

			// success response
			$content = array(
				'error' => false,
				'messages' => array('Challenge updated!'),
				'challenges' => array('author_id' => $challenge->author_id, 'description' => $challenge->description)
			);

		} else {

			// response
			$content = array(
				'error' => true,
				'messages' => $challenge->getErrors()
			);	

		}

		$response = Response::json($content, 200);
		return $response;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$challenge = Challenge::find($id);
		$current = DB::table('users')->where('token', '=', Input::get('token'))->pluck('id');

		//	
		if($challenge->author_id == $current && $challenge != NULL) {
			DB::table('participants')->where('challenge_id', '=', $id)->delete();
			DB::table('answers')->where('challenge_id', '=', $id)->delete();
			DB::table('votes')->where('item_id', '=', $id)->where('is_challenge', '=', 1)->delete();
			$challenge->delete();
			$content = array(
				'error' => false,
				'messages' => array('Challenge deleted!')
			);
		} else {
			$content = array(
				'error' => true,
				'messages' => "Fail!"
			);
		}

		$response = Response::json($content, 200);
		return $response;
	}

}