<?php

class FollowersController extends \BaseController {

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$input = Input::all();
		$input['user_id'] = DB::table('users')->where('token', '=', $input['token'])->pluck('id');
		$followers = DB::table('followers')
						->select('followers.user_from', 
				        		'followers.user_to', 
				        		'followers.created_at', 
				        		'followers.updated_at', 
				        		'users.id as user_id', 
				        		'users.username', 
				        		'users.name', 
				        		'users.picture',
				        		DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id IN (SELECT challenges.id FROM challenges WHERE challenges.id = followers.user_to)) as votes'))
						->where('user_from', '=', $input['user_id'])
						->leftJoin('users', 'users.id', '=', 'followers.user_to')
						->get();

		$following = DB::table('followers')
						->select('followers.user_from', 
				        		'followers.user_to', 
				        		'followers.created_at', 
				        		'followers.updated_at', 
				        		'users.id as user_id', 
				        		'users.username', 
				        		'users.name', 
				        		'users.picture',
				        		DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id IN (SELECT challenges.id FROM challenges WHERE challenges.id = followers.user_to)) as votes'))
						->where('user_to', '=', $input['user_id'])
						->leftJoin('users', 'users.id', '=', 'followers.user_to')
						->get();

		$content = array(
			'error' => false,
			'followers' =>$followers,
			'following' =>$following
		);

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$current = DB::table('users')->where('token', '=', $input['token'])->pluck('id');
		$input['user_from'] = $current;
		$following = new Follower;

		// process the login
		if ($following->validate($input)) {

			// store
			$following->user_from = $current;
			$following->user_to = $input['user_to'];
			$following->save();

			// success response
			$content = array(
				'error' => false,
				'messages' => array('Followed!'),
			);

		} else {

			// fail response
			$content = array(
				'error' => true,
				'messages' => $following->getErrors()
			);

		}

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Removes current users followers.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$current = DB::table('users')->where('token', '=', Input::get('token'))->pluck('id');
		$followers = DB::table('followers')->where('user_from', '=', $current)->where('user_to', '=', $id)->pluck('id');

		if($followers != NULL) {
			Follower::find($followers)->delete();
			$content = array(
				'error' => false,
				'messages' => array('Unfollowed!')
			);
		} else {
			$content = array(
				'error' => true,
				'messages' => array('Nothing to unfollow!')
			);
		}

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

}