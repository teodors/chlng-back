<?php

class AnswersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$answers = DB::table('answers')
						->select('answers.id', 
							'answers.challenge_id',
							'answers.description',
							'answers.created_at',
							'answers.updated_at',
							'users.id as user_id',
							'users.username',
							'users.name',
							'users.picture'
							)
						->leftJoin('users', 'users.id', '=', 'answers.user_id')
						->orderBy('answers.created_at', 'desc')
						->get();

		$content = array(
			'error' => false,
			'messages' => array(),
			'answers' => $answers
		);
		
		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$answers = new Answer;

		// process the login
		if ($answers->validate($input)) {

			// store
			$answers->user_id = DB::table('users')->where('token', '=', $input['token'])->pluck('id');
			$answers->challenge_id = Input::get('challenge_id');
			$answers->description = Input::get('description');
			$answers->save();

			// success response
			$content = array(
				'error' => false,
				'messages' => array('You answer has been added!'),
				'answers' => array(
					'id' => $answers->id, 
					'user_id' => $answers->user_id, 
					'challenge_id' => $answers->challenge_id,
					'description' => $answers->description)
			);

		} else {

			// fail response
			$content = array(
				'error' => true,
				'messages' => $answers->getErrors()
			);	

		}

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$results = Answer::find($id);
		$answers = DB::table('answers')
						->select('answers.id', 
							'answers.challenge_id',
							'answers.description',
							'answers.created_at',
							'answers.updated_at',
							'users.id as user_id',
							'users.username',
							'users.name',
							'users.picture'
							)
						->where('answers.id', '=', $id)
						->leftJoin('users', 'users.id', '=', 'answers.user_id')
						->get();

		$content = array(
			'error' => false,
			'messages' => array(),
			'answers' => $answers
		);

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$input['challenge_id'] = $id;
		$answers = Answer::find($id);

		// process the login
		if ($answers->validate($input) && $answers->user_id == DB::table('users')->where('token', '=', $input['token'])->pluck('id')) {

			// store
			$answers->description = Input::get('description');
			$answers->save();

			// success response
			$content = array(
				'error' => false,
				'messages' => array('Answer changed!'),
				'answers' => array('user_id' => $answers->user_id, 'challenge_id' => $answers->challenge_id, 'description' => $answers->description)
			);

		} else {

			// response
			$content = array(
				'error' => true,
				'messages' => $answers->getErrors()
			);	

		}

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$chalenges = Answer::find($id);
		$current = DB::table('users')->where('token', '=', Input::get('token'))->pluck('id');

		if($chalenges->user_id == $current && $chalenges != NULL) {
			$chalenges->delete();
			$content = array(
				'error' => false,
				'messages' => array('Answer deleted!')
			);
		} else {
			$content = array(
				'error' => true,
				'messages' => "Fail!"
			);
		}

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

}