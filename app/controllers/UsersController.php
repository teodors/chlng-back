<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if(Input::get('user_id') != NULL) {
			$results = DB::table('followers')
				->select(
					'users.id', 
					'users.username', 
					'users.name', 
					'users.picture',
					DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id IN (SELECT challenges.id FROM challenges WHERE challenges.id = users.id)) as votes'))
				->where('followers.user_from', '=', Input::get('user_id'))
				->leftJoin('users', 'followers.user_to', '=', 'users.id')				
				->get();	
		} else {
			$results = DB::table('users')
				->select(
					'users.id', 
					'users.username', 
					'users.name', 
					'users.picture',
					DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id IN (SELECT challenges.id FROM challenges WHERE challenges.id = users.id)) as votes'))
				->get();
		}

		$content = array(
			'error' => false,
			'messages' => array(),
			'users' => $results
		);
		
		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function login()
	{
		$input = Input::all();

		if(array_key_exists('username', $input) && array_key_exists('password', $input)) {
			$hashedPassword = DB::table('users')
	                	->where('username', '=', $input['username'])
	                	->pluck('password');

	        if (Hash::check($input['password'], $hashedPassword)) {
				$token = Hash('md5', time() . uniqid() . $input['username']);
				User::where('username', '=', $input['username'])->update(array('token' => $token));

				$results = DB::table('users')
				->select(
					'users.id', 
					'users.username', 
					'users.name', 
					'users.picture',
					'users.bio',
					'users.token',
					DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id IN (SELECT challenges.id FROM challenges WHERE challenges.author_id = users.id) AND votes.is_challenge = 1) as challenge_votes'),
					DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id IN (SELECT answers.id FROM answers WHERE answers.user_id = users.id) AND votes.is_challenge = 0) as answer_votes'))
				->where('username', '=', $input['username'])
				->get();

				$results[0]->votes = $results[0]->challenge_votes + $results[0]->answer_votes;
				$content = array(
					'error' => false,
					'messages' => array("Welcome! You have successfully logen-in."),
					'users' => $results
				);			
			} else {
				$content = array(
					'error' => true,
					'messages' => array("Damn! Your username and/or password is wrong."),
				);
			}			
		} else {
			$content = array(
				'error' => true,
				'messages' => array("Hey! Your are missing username and/or password."),
			);
		}
		
	    $response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Return possible gravatar
	 *
	 * @return Response
	 */
	public function gravatar($hash)
	{
		$content = array(
			'error' => false,
			'messages' => array(),
		);

		$str = @file_get_contents( 'http://www.gravatar.com/' . $hash . '.php' );
		if($str !== false) {
			$results = unserialize($str);
			$content['error'] = false;
			$content['name'] = $results['entry'][0]['name']['formatted'];
		} else {
			$content['message'] = array("Couldn't find Gravatar user.");
			$content['error'] = true;
		}

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Deletes session token.
	 *
	 * @return Response
	 */
	public function logout()
	{
		$token = Input::get('token');

		User::where('token', '=', $token)->update(array('token' => ''));

		$content = array(
			'error' => false,
			'messages' => array("You are now logged out.")
		);

	    $response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::json()->all();
		$user = new User;

		// process the login
		if ($user->validate($input)) {

			// store
			$user->name = $input['name'];
			$user->username = $input['username'];
			$user->email = $input['email'];
			$user->token = Hash('md5', time() . uniqid() . $user->username);

			$user->bio = Input::json()->get('bio');
			$user->password = Hash::make(Input::json()->get('password'));
			$user->picture = Input::json()->get('picture');
			$user->location = Input::json()->get('location');
			$user->web = Input::json()->get('web');
			
			$user->save();

			// success response
			$content = array(
				'error' => false,
				'messages' => array('Successfully created user.'),
				'users' => array(
					'id' => $user->id, 
					'username' => $user->username, 
					'name' => $user->name, 
					'picture' => $user->picture, 
					'votes' => '0', 
					'bio' => $user->bio, 
					'token' => $user->token)
			);

		} else {

			// response
			$content = array(
				'error' => true,
				'messages' => $user->getErrors()
			);	

		}

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		// time when originaly started request
		if(Input::get('date') == NULL) {
			$req_date = date('Y-m-d H:i:s');
		} else {
			$req_date = Input::get('date');
		}
		
		$results = DB::table('users')
				->select(
					'users.id', 
					'users.username', 
					'users.email', 
					'users.name', 
					'users.picture',
					'users.bio',
					'users.web',
					'users.location',
					DB::raw('(SELECT COUNT(*) FROM followers WHERE followers.user_from = users.id) as following'),
					DB::raw('(SELECT COUNT(*) FROM followers WHERE followers.user_to = users.id) as followers'),
					DB::raw('(SELECT COUNT(*) FROM challenges WHERE challenges.author_id = users.id) as challenges'),
					DB::raw('(SELECT COUNT(*) FROM participants WHERE participants.user_id = users.id) as wins'),
					DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id IN (SELECT challenges.id FROM challenges WHERE challenges.author_id = users.id) AND votes.is_challenge = 1) as challenge_votes'),
					DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id IN (SELECT answers.id FROM answers WHERE answers.user_id = users.id) AND votes.is_challenge = 0) as answer_votes'))
				->where('users.id', '=', $id)
				->get();

		$challenges = DB::table('challenges') // A
        	->select('challenges.id', 
        		'challenges.description', 
        		'challenges.created_at', 
        		'challenges.updated_at', 
        		'users.id as user_id', 
        		'users.username', 
        		'users.name', 
        		'users.picture',
        		DB::raw('(SELECT COUNT(*) FROM votes WHERE votes.item_id = challenges.id ) as votes'),     	
        		DB::raw('(SELECT COUNT(*) FROM answers WHERE answers.challenge_id = challenges.id ) as answers'))      	
	        ->leftJoin('users', 'users.id', '=', 'challenges.author_id')
		    ->where('author_id', '=', $id)
		    ->where('challenges.created_at', '<=', $req_date)
		    ->orderBy('challenges.created_at', 'desc')
		    // ->skip(1)
		    // ->take(1)		    
		    ->get();	

		$followers = DB::table('followers')
				->select(
					'users.id', 
					'users.username', 
					'users.name', 
					'users.picture')
				->where('user_to', '=', $id)
				->leftJoin('users', 'user_from', '=', 'users.id')
				->get();	

		$following = DB::table('followers')
				->select(
					'users.id', 
					'users.username', 
					'users.name', 
					'users.picture')
				->where('user_from', '=', $id)
				->leftJoin('users', 'user_to', '=', 'users.id')
				->get();			

		$results[0]->votes = $results[0]->challenge_votes + $results[0]->answer_votes;
		$content = array(
			'error' => false,
			'messages' => array(),
			'users' => $results,
			'following' => $following,
			'followers' => $followers,
			'challenges' => $challenges
		);	

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$user = User::find($id);		

		if ($user['attributes']['token'] === $input['token'] && $user->validate($input, $id)) {

			// store
			$user->name = Input::get('name');
			$user->username = Input::get('username');
			$user->email = Input::get('email');
			$user->bio = Input::get('bio');
			$user->picture = Input::get('picture');
			$user->location = Input::get('location');
			$user->web = Input::get('web');
			$user->save();

			// success response
			$content = array(
				'error' => false,
				'messages' => array('Profile updated!')
			);

		} else {

			// response
			$content = array(
				'error' => true,
				'messages' => $user->getErrors()
			);	

		}	

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);
		$current = DB::table('users')->where('token', '=', Input::get('token'))->pluck('id');

		if($id == $current && $user != NULL) {
			$user->delete();
			$content = array(
				'error' => false,
				'messages' => array('User deleted!')
			);
		} else {
			$content = array(
				'error' => true,
				'messages' => array('Fail!')
			);
		}

		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

}