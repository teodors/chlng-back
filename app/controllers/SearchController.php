<?php

class SearchController extends \BaseController {

	/**
	 * Display the search results
	 *
	 * @param  int  $search
	 * @return Response
	 */
	public function show($search)
	{
		$users = DB::table('users')
			->select('users.id', 
				'users.username',
				'users.name',
				'users.picture'
				)
			->where('username', 'LIKE', '%' . $search . '%')
			->orWhere('name', 'LIKE', '%' . $search . '%')
			->get();

		$challenges = DB::table('challenges')
			->select('challenges.id', 
        		'challenges.description', 
        		'challenges.created_at', 
        		'challenges.updated_at', 
        		'users.id as user_id', 
        		'users.username', 
        		'users.name', 
        		'users.picture')
	        ->leftJoin('users', 'users.id', '=', 'challenges.author_id')
			->where('challenges.description', 'LIKE', '%' . $search . '%')
			->get();

		// creates response data
		$content = array(
			'error' => false,
			'messages' => array(),
			'users' => $users,
			'challenges' => $challenges
		);

		// prepare response (encodes to JSON) and adds header
		$response = Response::json($content, 200);
		$response->headers->add(array('Access-Control-Allow-Origin' => '*'));

		return $response;
	}

}
