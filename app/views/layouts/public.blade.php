<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Challen.gr</title>
    <meta name="description" content="Challenge your friends!">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Authors -->
    <link type="text/plain" rel="author" href="humans.txt" />

    <!-- Fav & Touch Icon -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="apple-touch-icon" href="img/touch/apple-touch-icon-iphone.png" /> 
    <link rel="apple-touch-icon" sizes="72x72" href="img/touch/apple-touch-icon-ipad.png" /> 
    <link rel="apple-touch-icon" sizes="114x114" href="img/touch/apple-touch-icon-iphone4.png" />

    <!-- CSS -->
    <link href="http://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic&subset=latin,latin-ext" rel="stylesheet" type="text/css">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" type="text/css">
	{{ HTML::style("css/style.css") }}
	<!--[if lt IE 9]>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
		<script src="htpp://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script><![endif]-->

    <!-- JS -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script> 
    <script src="js/vendor/jquery.easing-1.3.pack.js"></script>    
    <script src="js/general.js"></script>
</head>
<body>
	
	@yield("content")

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
	{{ HTML::script("js/main.js") }}
</body>
</html>
